# Fund sales

This project is generated with [yo angular webapp](https://github.com/yeoman/generator-webapp)
version 0.11.1.

## CSS
リファレンス[bootstrap3](http://getbootstrap.com/css/)

## Chart tool
[highcharts](http://www.highcharts.com/)

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.

## development
`SoruceTree`gitのクライアントツールのインストール
https://www.sourcetreeapp.com/

`node install` する
https://nodejs.org/
```
git clone リポジトリ

cd fund_sales

npm install 

sudo npm install -g bower
sudo npm install -g grunt-cli

bower install

grunt serve
```

## production deploy


```

grunt build
```
