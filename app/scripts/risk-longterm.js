'use strict';
$(function () {
  var chart;
  var data = {
    series: [
      {name: "リターン",visible: false,color: '#E49B47',pointStart: 1997,pointInterval: 1,data: [12.58, 2.90, 8.31, -1.07, 1.91, -9.42, 10.72, 7.38, 17.52, 12.09, 1.67, -22.89, 16.98, -4.26, -5.03, 20.94, 35.44, 15.96]},
      {name: "リターン",visible: false,color: '#E49B47',pointStart: 1997,pointInterval: 1,data: [7.92, 5.73, 3.58, 0.41, -3.84, 0.15, 9.45, 13.10, 15.87, 6.99, -10.80, -4.90, 6.00, -4.53, 7.43, 31.90, 28.53, 0]},
      {name: "リターン",visible: false,color: '#E49B47',pointStart: 1997,pointInterval: 1,data: [8.49, 3.42, 3.07, -2.89, 0.74, 2.57, 13.24, 13.82, 11.31, -4.04, -2.76, -4.55, 2.12, 3.32, 18.52, 29.98, 0, 0]},
      {name: "リターン",visible: false,color: '#E49B47',pointStart: 1997,pointInterval: 1,data: [6.03, 3.09, -0.27, 0.28, 2.44, 6.64, 14.16, 10.96, 0.82, 0.70, -3.05, -4.50, 7.16, 12.24, 20.10, 0, 0, 0]},
      {name: "リターン",visible: false,color: '#E49B47',pointStart: 1997,pointInterval: 1,data: [5.30, 0.36, 1.90, 1.72, 5.80, 8.38, 11.85, 2.18, 4.16, -0.32, -3.32, -0.16, 14.85, 14.54, 0, 0, 0, 0]},
      {name: "リターン",visible: false,color: '#E49B47',pointStart: 1997,pointInterval: 1,data: [2.43, 2.12, 2.93, 4.60, 7.43, 7.38, 3.80, 4.96, 2.61, -1.09, 0.14, 5.72, 17.01, 0, 0, 0, 0, 0]},
      {name: "リターン",visible: false,color: '#E49B47',pointStart: 1997,pointInterval: 1,data: [3.84, 3.00, 5.46, 6.15, 6.71, 1.60, 6.23, 3.46, 1.41, 1.86, 5.23, 7.97, 0, 0, 0, 0, 0, 0]},
      {name: "リターン",visible: false,color: '#E49B47',pointStart: 1997,pointInterval: 1,data: [4.53, 5.28, 6.87, 5.68, 1.67, 3.76, 4.69, 2.25, 4.11, 6.64, 7.30, 0, 0, 0, 0, 0, 0, 0]},
      {name: "リターン",visible: false,color: '#E49B47',pointStart: 1997,pointInterval: 1,data: [6.68, 6.60, 6.39, 1.35, 3.62, 2.73, 3.40, 4.74, 8.88, 8.62, 0, 0, 0, 0, 0, 0, 0, 0]},
      {name: "リターン",visible: false,color: '#E49B47',pointStart: 1997,pointInterval: 1,data: [7.95, 6.21, 2.15, 3.12, 2.70, 1.83, 5.80, 9.32, 10.87, 0, 0, 0, 0, 0, 0, 0, 0, 0]}
    ]
  };
  var y_axis_min = 0, y_axis_max = 0;
  for (var i = 0; i < data.series.length; i++) {
    y_axis_min = Math.min(y_axis_min, Math.min.apply(null, data.series[i].data));
    y_axis_max = Math.max(y_axis_max, Math.max.apply(null, data.series[i].data));
  }

  $(document).ready(function () {
    var sliderLabel = $("#data");

    chart = new Highcharts.Chart({
      chart: {
        renderTo: 'container2',
        type: 'column',
        hight: 390,
        backgroundColor: null
      },
      colors: ['#E49B47'],
      title: {
        text: ' '
      },
      credits: {
        text: false
      },
      exporting: {enabled: false},
      //plotOptions: {
      //  column: {stacking: 'normal'}
      //},
      legend: {enabled: false},
      xAxis: {
        title: {
          text: '(年)'
        },
        labels: {
          style: {
            fontSize: '15px'
          }
        },
        allowDecimals: true
      },
      tooltip: {
        formatter: function() {
          return ''+
            this.x +': '+ this.y + '%';
        }
      },
      yAxis: {
        labels: {
          align: 'left',
          x: -5,
          y: 5,
          style: {
            fontSize:'15px'
          },
          formatter: function() {
            return (this.value + '%');
          }
        },
        title: {
          text: ''
        },
        max: y_axis_max,
        min: y_axis_min
      },
      series: data.series
    });
    chart.series[0].show();

    $('#slider').slider({
      range: "min",
      min: 0,
      max: 9,
      step: 1,
      slide: function (event, ui) {
        for(var i=0;i < chart.series.length; i++){
          if(chart.series[i].visible) {
            chart.series[i].hide();
          }
        }
        chart.series[ui.value].show();
        sliderLabel.html(ui.value + 1);
      }
    });

    $('select.styled').customSelect();

  });
})
;
