'use strict';
$(function () {
  var chart;
  var data = {
    series: [
      {type:'column',visible:true, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最大リターン：</b><b style="color:#7cb5ec; font-weight: bold;">{point.stackTotal}%</b>'},
        data:[
          {y:6.74,name:"1997/3/31",color:"#7cb5ec"},{y:65.04,name:"2013/9/30",color:"#7cb5ec"},{y:31.51,name:"1998/6/30",color:"#7cb5ec"},{y:54.92,name:"1998/6/30",color:"#7cb5ec"},{y:35.16,name:"2013/9/30",color:"#7cb5ec"}
        ]},
      {type:'column',visible:true, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最小リターン：</b><b style="color:#dddee0; font-weight: bold;">{point.y}%</b>'},
        data:[
          {y:-2.94,name:"2006/6/30",color:"#dddee0"},{y:-40.61,name:"2008/12/31",color:"#dddee0"},{y:-25.87,name:"1999/9/30",color:"#dddee0"},{y:-43.42,name:"2009/3/31",color:"#dddee0"},{y:-21.68,name:"2009/3/31",color:"#dddee0"}
        ]},
      {type:'spline',visible:true,lineWidth:0,color: '#e49941',marker:{radius:10,symbol:"square"},tooltip: {headerFormat:'',pointFormat: '<b style="font-weight: bold;">運用期間平均リターン：</b><b style="font-weight: bold;">{point.y}%</b>'},
        data:[2.34,3.03,6.61,12.08,5.81]},
      {type:'column',visible:false, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最大リターン：</b><b style="color:#7cb5ec; font-weight: bold;">{point.stackTotal}%</b>'},
        data:[
          {y:6.67,name:"1997/9/30",color:"#7cb5ec"},{y:43.51,name:"2013/9/30",color:"#7cb5ec"},{y:24.33,name:"2013/9/30",color:"#7cb5ec"},{y:56.53,name:"1997/6/30",color:"#7cb5ec"},{y:28.5,name:"2012/12/31",color:"#7cb5ec"}
        ]},
      {type:'column',visible:false, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最小リターン：</b><b style="color:#dddee0; font-weight: bold;">{point.y}%</b>'},
        data:[
          {y:-0.56,name:"2006/6/30",color:"#dddee0"},{y:-26.54,name:"2008/3/31",color:"#dddee0"},{y:-14.28,name:"1999/9/30",color:"#dddee0"},{y:-17.26,name:"2009/6/30",color:"#dddee0"},{y:-11.88,name:"2008/3/31",color:"#dddee0"}
        ]},
      {type:'spline',visible:false,lineWidth:0,color: '#e49941',marker:{radius:10,symbol:"square"},tooltip: {headerFormat:'',pointFormat: '<b style="font-weight: bold;">運用期間平均リターン：</b><b style="font-weight: bold;">{point.y}%</b>'},
        data:[2.25,3.45,6.42,11.65,5.68]},
      {type:'column',visible:false, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最大リターン：</b><b style="color:#7cb5ec; font-weight: bold;">{point.stackTotal}%</b>'},
        data:[
          {y:4.98,name:"1997/3/31",color:"#7cb5ec"},{y:42.21,name:"2004/3/31",color:"#7cb5ec"},{y:24.14,name:"2012/12/31",color:"#7cb5ec"},{y:48.05,name:"2012/12/31",color:"#7cb5ec"},{y:25.64,name:"2012/12/31",color:"#7cb5ec"}
        ]},
      {type:'column',visible:false, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最小リターン：</b><b style="color:#dddee0; font-weight: bold;">{point.y}%</b>'},
        data:[
          {y:-0.42,name:"2004/6/30",color:"#dddee0"},{y:-17.65,name:"2007/3/31",color:"#dddee0"},{y:-6.66,name:"2008/12/31",color:"#dddee0"},{y:-11.01,name:"2001/3/31",color:"#dddee0"},{y:-7.24,name:"2008/6/30",color:"#dddee0"}
        ]},
      {type:'spline',visible:false,lineWidth:0,color: '#e49941',marker:{radius:10,symbol:"square"},tooltip: {headerFormat:'',pointFormat: '<b style="font-weight: bold;">運用期間平均リターン：</b><b style="font-weight: bold;">{point.y}%</b>'},
        data:[2.18,2.57,5.56,9.87,4.98]},
      {type:'column',visible:false, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最大リターン：</b><b style="color:#7cb5ec; font-weight: bold;">{point.stackTotal}%</b>'},
        data:[
          {y:4.19,name:"1997/3/31",color:"#7cb5ec"},{y:31.82,name:"2004/3/31",color:"#7cb5ec"},{y:18.13,name:"2011/12/31",color:"#7cb5ec"},{y:30.5,name:"2011/12/31",color:"#7cb5ec"},{y:16.69,name:"2011/12/31",color:"#7cb5ec"}
        ]},
      {type:'column',visible:false, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最小リターン：</b><b style="color:#dddee0; font-weight: bold;">{point.y}%</b>'},
        data:[
          {y:0.15,name:"2004/6/30",color:"#7cb5ec"},{y:-12.23,name:"2008/9/30",color:"#dddee0"},{y:-4.98,name:"2008/12/31",color:"#dddee0"},{y:-7.69,name:"2000/3/31",color:"#dddee0"},{y:-5.32,name:"2008/9/30",color:"#dddee0"}
        ]},
      {type:'spline',visible:false,lineWidth:0,color: '#e49941',marker:{radius:10,symbol:"square"},tooltip: {headerFormat:'',pointFormat: '<b style="font-weight: bold;">運用期間平均リターン：</b><b style="font-weight: bold;">{point.y}%</b>'},
        data:[2.15,1.3,5.36,8.69,4.44]},
      {type:'column',visible:false, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最大リターン：</b><b style="color:#7cb5ec; font-weight: bold;">{point.stackTotal}%</b>'},
        data:[
          {y:4.16,name:"1997/6/30",color:"#7cb5ec"},{y:17.17,name:"2003/9/30",color:"#7cb5ec"},{y:15.81,name:"2001/9/30",color:"#7cb5ec"},{y:31.15,name:"2010/3/31",color:"#7cb5ec"},{y:12.89,name:"2010/3/31",color:"#7cb5ec"}
        ]},
      {type:'column',visible:false, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最小リターン：</b><b style="color:#dddee0; font-weight: bold;">{point.y}%</b>'},
        data:[
          {y:0.49,name:"2002/6/30",color:"#7cb5ec"},{y:-10.48,name:"2007/12/31",color:"#dddee0"},{y:-3.34,name:"2007/12/31",color:"#dddee0"},{y:-6.13,name:"1999/3/31",color:"#dddee0"},{y:-3.61,name:"2007/12/31",color:"#dddee0"}
        ]},
      {type:'spline',visible:false,lineWidth:0,color: '#e49941',marker:{radius:10,symbol:"square"},tooltip: {headerFormat:'',pointFormat: '<b style="font-weight: bold;">運用期間平均リターン：</b><b style="font-weight: bold;">{point.y}%</b>'},
        data:[2.14,0.35,5.61,7.84,4.16]},
      {type:'column',visible:false, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最大リターン：</b><b style="color:#7cb5ec; font-weight: bold;">{point.stackTotal}%</b>'},
        data:[
          {y:3.35,name:"1997/6/30",color:"#7cb5ec"},{y:14.26,name:"2009/12/31",color:"#7cb5ec"},{y:15.9,name:"2001/9/30",color:"#7cb5ec"},{y:32.81,name:"2009/12/31",color:"#7cb5ec"},{y:14.13,name:"2009/12/31",color:"#7cb5ec"}
        ]},
      {type:'column',visible:false, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最小リターン：</b><b style="color:#dddee0; font-weight: bold;">{point.y}%</b>'},
        data:[
          {y:0.72,name:"2002/6/30",color:"#7cb5ec"},{y:-8.49,name:"2006/12/31",color:"#dddee0"},{y:-1.39,name:"2006/12/31",color:"#dddee0"},{y:-2.33,name:"1999/6/30",color:"#dddee0"},{y:-1.77,name:"2006/12/31",color:"#dddee0"}
        ]},
      {type:'spline',visible:false,lineWidth:0,color: '#e49941',marker:{radius:10,symbol:"square"},tooltip: {headerFormat:'',pointFormat: '<b style="font-weight: bold;">運用期間平均リターン：</b><b style="font-weight: bold;">{point.y}%</b>'},
        data:[2.09,-0.52,5.84,6.7,3.9]},
      {type:'column',visible:false, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最大リターン：</b><b style="color:#7cb5ec; font-weight: bold;">{point.stackTotal}%</b>'},
        data:[
          {y:3.13,name:"1997/3/31",color:"#7cb5ec"},{y:8.89,name:"1999/12/31",color:"#7cb5ec"},{y:15.76,name:"2001/6/30",color:"#7cb5ec"},{y:15.93,name:"2004/3/31",color:"#7cb5ec"},{y:7.15,name:"2001/6/30",color:"#7cb5ec"}
        ]},
      {type:'column',visible:false, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最小リターン：</b><b style="color:#dddee0; font-weight: bold;">{point.y}%</b>'},
        data:[
          {y:1.06,name:"2002/6/30",color:"#7cb5ec"},{y:-6.97,name:"1997/3/31",color:"#dddee0"},{y:0.01,name:"2006/9/30",color:"#7cb5ec"},{y:-0.34,name:"2003/3/31",color:"#dddee0"},{y:0.29,name:"2006/9/30",color:"#7cb5ec"}
        ]},
      {type:'spline',visible:false,lineWidth:0,color: '#e49941',marker:{radius:10,symbol:"square"},tooltip: {headerFormat:'',pointFormat: '<b style="font-weight: bold;">運用期間平均リターン：</b><b style="font-weight: bold;">{point.y}%</b>'},
        data:[2.04,-0.58,6.04,7.01,4.07]},
      {type:'column',visible:false, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最大リターン：</b><b style="color:#7cb5ec; font-weight: bold;">{point.stackTotal}%</b>'},
        data:[
          {y:2.13,name:"1997/3/31",color:"#7cb5ec"},{y:8.39,name:"1999/12/31",color:"#7cb5ec"},{y:13.89,name:"2000/12/31",color:"#7cb5ec"},{y:14.33,name:"2004/3/31",color:"#7cb5ec"},{y:6.17,name:"1999/12/31",color:"#7cb5ec"}
        ]},
      {type:'column',visible:false, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最小リターン：</b><b style="color:#dddee0; font-weight: bold;">{point.y}%</b>'},
        data:[
          {y:1.25,name:"2004/3/31",color:"#7cb5ec"},{y:-4.1,name:"2002/3/31",color:"#dddee0"},{y:0.93,name:"2004/6/30",color:"#7cb5ec"},{y:0.2,name:"2001/12/31",color:"#7cb5ec"},{y:1.39,name:"2002/3/31",color:"#7cb5ec"}
        ]},
      {type:'spline',visible:false,lineWidth:0,color: '#e49941',marker:{radius:10,symbol:"square"},tooltip: {headerFormat:'',pointFormat: '<b style="font-weight: bold;">運用期間平均リターン：</b><b style="font-weight: bold;">{point.y}%</b>'},
        data:[2,0.07,6.23,8.04,4.53]},
      {type:'column',visible:false, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最大リターン：</b><b style="color:#7cb5ec; font-weight: bold;">{point.stackTotal}%</b>'},
        data:[
          {y:1.98,name:"1997/6/30",color:"#7cb5ec"},{y:7.71,name:"1999/9/30",color:"#7cb5ec"},{y:9.61,name:"2001/3/31",color:"#7cb5ec"},{y:18.32,name:"2005/12/31",color:"#7cb5ec"},{y:6.43,name:"2005/12/31",color:"#7cb5ec"}
        ]},
      {type:'column',visible:false, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最小リターン：</b><b style="color:#dddee0; font-weight: bold;">{point.y}%</b>'},
        data:[
          {y:1.37,name:"1999/9/30",color:"#7cb5ec"},{y:-5.48,name:"2001/3/31",color:"#dddee0"},{y:0.92,name:"2004/6/30",color:"#7cb5ec"},{y:-0.37,name:"2001/3/31",color:"#dddee0"},{y:1.11,name:"2001/3/31",color:"#7cb5ec"}
        ]},
      {type:'spline',visible:false,lineWidth:0,color: '#e49941',marker:{radius:10,symbol:"square"},tooltip: {headerFormat:'',pointFormat: '<b style="font-weight: bold;">運用期間平均リターン：</b><b style="font-weight: bold;">{point.y}%</b>'},
        data:[1.98,0.56,6.41,9.05,4.93]},
      {type:'column',visible:false, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最大リターン：</b><b style="color:#7cb5ec; font-weight: bold;">{point.stackTotal}%</b>'},
        data:[
          {y:1.3,name:"1997/3/31",color:"#7cb5ec"},{y:5.49,name:"2004/3/31",color:"#7cb5ec"},{y:8.8,name:"1998/6/30",color:"#7cb5ec"},{y:21.8,name:"2005/12/31",color:"#7cb5ec"},{y:7.36,name:"2005/12/31",color:"#7cb5ec"}
        ]},
      {type:'column',visible:false, tooltip: {pointFormat: '<b style="font-weight: bold;">運用期間最小リターン：</b><b style="color:#dddee0; font-weight: bold;">{point.y}%</b>'},
        data:[
          {y:1.49,name:"1999/9/30",color:"#7cb5ec"},{y:-4.05,name:"2000/12/31",color:"#dddee0"},{y:2.44,name:"2003/9/30",color:"#7cb5ec"},{y:0,name:"2000/3/31",color:"#dddee0"},{y:1.55,name:"2000/3/31",color:"#7cb5ec"}
        ]},
      {type:'spline',visible:false,lineWidth:0,color: '#e49941',marker:{radius:10,symbol:"square"},tooltip: {headerFormat:'',pointFormat: '<b style="font-weight: bold;">運用期間平均リターン：</b><b style="font-weight: bold;">{point.y}%</b>'},
        data:[1.99,0.56,6.53,9.88,5.12]}
    ]
  };
  var y_axis_min = 0, y_axis_max = 0;
  for (var i = 0; i < data.series.length; i++) {
    y_axis_min = Math.min(y_axis_min, Math.min.apply(null, data.series[i].data));
    y_axis_max = Math.max(y_axis_max, Math.max.apply(null, data.series[i].data));
  }

  $(document).ready(function () {
    var sliderLabel = $("#data");

    chart = new Highcharts.Chart({
      chart: {
        renderTo: 'container4',
        hight: 390,
        backgroundColor: null
      },
      title: {text: null},
      credits: {text: false},
      exporting: {enabled: false},
      xAxis: {categories: ['国内債券', '国内株式', '外国債券', '外国株式', '分散投資']},
      yAxis: {title: {text: '（％）'},min: -60,max: 80},
      legend: {enabled: false},
      plotOptions: {column: {stacking: 'normal'}},
      animation: false,
      series: data.series
    });
    chart.series[0].show();

    $('#slider').slider({
      range: "min",
      min: 0,
      max: 9,
      step: 1,
      slide: function (event, ui) {
        for(var i=0;i < chart.series.length; i++){
          if(chart.series[i].visible) {
            chart.series[i].hide();
          }
        }
        chart.series[ui.value*3].show();
        chart.series[ui.value*3+1].show();
        chart.series[ui.value*3+2].show();
        sliderLabel.html(ui.value + 1);
      }
    });

    $('select.styled').customSelect();

  });
})
;
