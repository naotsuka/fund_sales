'use strict';
$(function () {
  var chart;
  var title = 'title';
  var fundRetern = {
    rateBank: {name: '預金', value: 0.0002, color: ['#C6B68B']},
    rate10: {name: '１０％', value: 0.1, color: '#b06d68'},
    rate9: {name: '９％', value: 0.09, color: '#b06d68'},
    rate8: {name: '８％', value: 0.08, color: '#b06d68'},
    rate7: {name: '７％', value: 0.07, color: '#b06d68'},
    rate6: {name: '６％', value: 0.06, color: '#b06d68'},
    rate5: {name: '５％', value: 0.05, color: '#b06d68'},
    rate4: {name: '４％', value: 0.04, color: '#b06d68'},
    rate3: {name: '３％', value: 0.03, color: '#b06d68'},
    rate2: {name: '２％', value: 0.02, color: '#b06d68'},
    rate1: {name: '１％', value: 0.01, color: '#b06d68'}
  };
  var activeRate = 10;

  // one year return calc
  var returnCalc = function (rate, value) {
    var _rate = rate,
      _value = value;
    var ans = Math.floor(_value * _rate * 100) / 100;
    return ans;
  };

  $(document).ready(function () {
    var toggleRate = $("#data");
    var yokinRate = $("#yokinRate");
    var fundRate = $("#fundRate");
    var returnRate = $("#returnRate");

    // Build the chart
    $('#container').highcharts({
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        width: 250,
        height: 250,
        backgroundColor: null
      },
      colors: ['#C6B68B','#b06d68'],
      title: {
        text: false
      },
      credits: {
        text: false
      },
      legend: {
        enabled: false
      },
      exporting: {enabled: false},
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: true
        }
      },
      series: [{
        type: 'pie',
        name: '比率',
        data: [
          ['預金', 100.0, '#C6B68B'],
        ],
        innerSize: '55%'
      }]
    });
    chart = $('#container').highcharts();
    $('#slider').slider({
      orientation: "horizontal",
      range: 'min',
      min: 0,
      max: 100,
      value: 0,
      step: 5,
      slide: function (event, ui) {
        var series = chart.series[0];
        series.setData([
          [fundRetern.rateBank.name, 100 - ui.value],
          [fundRetern['rate' + activeRate].name, ui.value]
        ]);
        var value1 = returnCalc(fundRetern['rate' + activeRate].value, ui.value);
        var value2 = returnCalc(fundRetern.rateBank.value, 100 - ui.value);

        fundRate.html(ui.value);
        yokinRate.html(100 - ui.value);
        returnRate.html(Math.floor(((value1 * 100) + (value2 * 100))) / 100);
      }
    });
    // The button action
    $('.pie-rate-button').click(function () {
      activeRate = this.attributes.value.value;
      var series = chart.series[0];
      var value = $('#slider').slider('value');
      series.setData([
        [fundRetern.rateBank.name, 100 - value],
        [fundRetern['rate' + activeRate].name, value]
      ]);
      var value1 = returnCalc(fundRetern['rate' + activeRate].value, value);
      var value2 = returnCalc(fundRetern.rateBank.value, 100 - value);
      returnRate.html(Math.floor(((value1 * 100) + (value2 * 100))) / 100);
      toggleRate.html(activeRate);
    });
  });
});
