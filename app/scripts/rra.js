'use strict';
$(function () {
  var activeRate = 10;
  var chart;
  var data = {
    series: [
      {name: '3%リターン',visible: false,color: '#7cb5ec',animation: false,marker: {symbol: "circle"},data: [['スタート',100.00],['1年後',103.00],['2年後',106.09],['3年後',109.27],['4年後',112.55],['5年後',115.93]]},
      {name: '3%リスクレンジ',visible: false,color: '#7cb5ec',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',108.69,97.31],['2年後',114.14,98.04],['3年後',119.13,99.41],['4年後',123.94,101.16],['5年後',128.66,103.20]]},
      {name: '4%リターン',visible: false,color: '#7cb5ec',animation: false,marker: {symbol: "circle"},data: [['スタート',100.00],['1年後',104.00],['2年後',108.16],['3年後',112.49],['4年後',116.99],['5年後',121.67]]},
      {name: '4%リスクレンジ',visible: false,color: '#7cb5ec',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',110.69,97.31],['2年後',117.62,98.70],['3年後',124.07,100.91],['4年後',130.36,103.61],['5年後',136.62,106.72]]},
      {name: '5%リターン',visible: false,color: '#7cb5ec',animation: false,marker: {symbol: "circle"},data: [['スタート',100.00],['1年後',105.00],['2年後',110.25],['3年後',115.76],['4年後',121.55],['5年後',127.63]]},
      {name: '5%リスクレンジ',visible: false,color: '#7cb5ec',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',114.87,95.13],['2年後',124.20,96.30],['3年後',132.85,98.67],['4年後',141.28,101.82],['5年後',149.69,105.56]]},
      {name: '6%リターン',visible: false,color: '#7cb5ec',animation: false,marker: {symbol: "circle"},data: [['スタート',100.00],['1年後',106.00],['2年後',112.36],['3年後',119.10],['4年後',126.25],['5年後',133.82]]},
      {name: '6%リスクレンジ',visible: false,color: '#7cb5ec',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',119.20,92.80],['2年後',131.03,93.69],['3年後',141.96,96.24],['4年後',152.64,99.85],['5年後',163.33,104.31]]},
      {name: '7%リターン',visible: false,color: '#7cb5ec',animation: false,marker: {symbol: "circle"},data: [['スタート',100.00],['1年後',107.00],['2年後',114.49],['3年後',122.50],['4年後',131.08],['5年後',140.26]]},
      {name: '7%リスクレンジ',visible: false,color: '#7cb5ec',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',123.59,90.41],['2年後',137.95,91.03],['3年後',151.24,93.77],['4年後',164.26,97.90],['5年後',177.35,103.16]]},
      {name: '8%リターン',visible: false,color: '#7cb5ec',animation: false,marker: {symbol: "circle"},data: [['スタート',100.00],['1年後',108.00],['2年後',116.64],['3年後',125.97],['4年後',136.05],['5年後',146.93]]},
      {name: '8%リスクレンジ',visible: false,color: '#7cb5ec',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',128.01,87.99],['2年後',144.94,88.34],['3年後',160.63,91.31],['4年後',176.07,96.03],['5年後',191.67,102.19]]},
      {name: '9%リターン',visible: false,color: '#7cb5ec',animation: false,marker: {symbol: "circle"},data: [['スタート',100.00],['1年後',109.00],['2年後',118.81],['3年後',129.50],['4年後',141.16],['5年後',153.86]]},
      {name: '9%リスクレンジ',visible: false,color: '#7cb5ec',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',132.59,85.41],['2年後',152.17,85.45],['3年後',170.36,88.64],['4年後',188.34,93.98],['5年後',206.61,101.11]]},
      {name: '10%リターン',visible: false,color: '#7cb5ec',animation: false,marker: {symbol: "circle"},data: [['スタート',100.00],['1年後',110.00],['2年後',121.00],['3年後',133.10],['4年後',146.41],['5年後',161.05]]},
      {name: '10%リスクレンジ',visible: false,color: '#7cb5ec',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',138.80,81.20],['2年後',161.72,80.28],['3年後',182.98,83.22],['4年後',204.00,88.82],['5年後',225.44,96.66]]},
      {name: '11%リターン',visible: false,color: '#7cb5ec',animation: false,marker: {symbol: "circle"},data: [['スタート',100.00],['1年後',111.00],['2年後',123.21],['3年後',136.76],['4年後',151.81],['5年後',168.51]]},
      {name: '11%リスクレンジ',visible: false,color: '#7cb5ec',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',146.65,75.35],['2年後',173.62,72.80],['3年後',198.51,75.02],['4年後',223.10,80.51],['5年後',248.22,88.80]]},
      {name: '12%リターン',visible: false,color: '#7cb5ec',animation: false,marker: {symbol: "circle"},data: [['スタート',100.00],['1年後',112.00],['2年後',125.44],['3年後',140.49],['4年後',157.35],['5年後',176.23]]},
      {name: '12%リスクレンジ',visible: false,color: '#7cb5ec',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',155.37,68.63],['2年後',186.78,64.10],['3年後',215.61,65.37],['4年後',244.09,70.61],['5年後',273.22,79.25]]},
      {name: '3%リターン',visible: false,color: '#e49941',animation: false,marker: {symbol: "square"},data: [['スタート',100.00],['1年後',103.00],['2年後',106.09],['3年後',109.27],['4年後',112.55],['5年後',115.93]]},
      {name: '3%リスクレンジ',visible: false,color: '#e49941',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',108.69,97.31],['2年後',114.14,98.04],['3年後',119.13,99.41],['4年後',123.94,101.16],['5年後',128.66,103.20]]},
      {name: '4%リターン',visible: false,color: '#e49941',animation: false,marker: {symbol: "square"},data: [['スタート',100.00],['1年後',104.00],['2年後',108.16],['3年後',112.49],['4年後',116.99],['5年後',121.67]]},
      {name: '4%リスクレンジ',visible: false,color: '#e49941',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',110.69,97.31],['2年後',117.62,98.70],['3年後',124.07,100.91],['4年後',130.36,103.61],['5年後',136.62,106.72]]},
      {name: '5%リターン',visible: false,color: '#e49941',animation: false,marker: {symbol: "square"},data: [['スタート',100.00],['1年後',105.00],['2年後',110.25],['3年後',115.76],['4年後',121.55],['5年後',127.63]]},
      {name: '5%リスクレンジ',visible: false,color: '#e49941',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',114.87,95.13],['2年後',124.20,96.30],['3年後',132.85,98.67],['4年後',141.28,101.82],['5年後',149.69,105.56]]},
      {name: '6%リターン',visible: false,color: '#e49941',animation: false,marker: {symbol: "square"},data: [['スタート',100.00],['1年後',106.00],['2年後',112.36],['3年後',119.10],['4年後',126.25],['5年後',133.82]]},
      {name: '6%リスクレンジ',visible: false,color: '#e49941',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',119.20,92.80],['2年後',131.03,93.69],['3年後',141.96,96.24],['4年後',152.64,99.85],['5年後',163.33,104.31]]},
      {name: '7%リターン',visible: false,color: '#e49941',animation: false,marker: {symbol: "square"},data: [['スタート',100.00],['1年後',107.00],['2年後',114.49],['3年後',122.50],['4年後',131.08],['5年後',140.26]]},
      {name: '7%リスクレンジ',visible: false,color: '#e49941',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',123.59,90.41],['2年後',137.95,91.03],['3年後',151.24,93.77],['4年後',164.26,97.90],['5年後',177.35,103.16]]},
      {name: '8%リターン',visible: false,color: '#e49941',animation: false,marker: {symbol: "square"},data: [['スタート',100.00],['1年後',108.00],['2年後',116.64],['3年後',125.97],['4年後',136.05],['5年後',146.93]]},
      {name: '8%リスクレンジ',visible: false,color: '#e49941',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',128.01,87.99],['2年後',144.94,88.34],['3年後',160.63,91.31],['4年後',176.07,96.03],['5年後',191.67,102.19]]},
      {name: '9%リターン',visible: false,color: '#e49941',animation: false,marker: {symbol: "square"},data: [['スタート',100.00],['1年後',109.00],['2年後',118.81],['3年後',129.50],['4年後',141.16],['5年後',153.86]]},
      {name: '9%リスクレンジ',visible: false,color: '#e49941',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',132.59,85.41],['2年後',152.17,85.45],['3年後',170.36,88.64],['4年後',188.34,93.98],['5年後',206.61,101.11]]},
      {name: '10%リターン',visible: false,color: '#e49941',animation: false,marker: {symbol: "square"},data: [['スタート',100.00],['1年後',110.00],['2年後',121.00],['3年後',133.10],['4年後',146.41],['5年後',161.05]]},
      {name: '10%リスクレンジ',visible: false,color: '#e49941',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',138.80,81.20],['2年後',161.72,80.28],['3年後',182.98,83.22],['4年後',204.00,88.82],['5年後',225.44,96.66]]},
      {name: '11%リターン',visible: false,color: '#e49941',animation: false,marker: {symbol: "square"},data: [['スタート',100.00],['1年後',111.00],['2年後',123.21],['3年後',136.76],['4年後',151.81],['5年後',168.51]]},
      {name: '11%リスクレンジ',visible: false,color: '#e49941',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',146.65,75.35],['2年後',173.62,72.80],['3年後',198.51,75.02],['4年後',223.10,80.51],['5年後',248.22,88.80]]},
      {name: '12%リターン',visible: false,color: '#e49941',animation: false,marker: {symbol: "square"},data: [['スタート',100.00],['1年後',112.00],['2年後',125.44],['3年後',140.49],['4年後',157.35],['5年後',176.23]]},
      {name: '12%リスクレンジ',visible: false,color: '#e49941',type: 'arearange',lineWidth: 0,fillOpacity: 0.1,zIndex: 0,data:[['スタート',100.00,100.00],['1年後',155.37,68.63],['2年後',186.78,64.10],['3年後',215.61,65.37],['4年後',244.09,70.61],['5年後',273.22,79.25]]}
    ]
  };

  $(document).ready(function () {
    // Build the chart

    chart = new Highcharts.Chart({
      chart: {
        renderTo: 'container3',
        hight: 390,
        backgroundColor: null
      },
      title: {text: null},
      credits: {text: false},
      exporting: {enabled: false},
      xAxis: {type: String,title: {text: 'N年後'}},
      yAxis: {title: {text: '（％）'}},
      tooltip: {crosshairs: true,shared: true,valueSuffix: '%'},
      legend: {enabled: false},
      series: data.series
    });
    chart.series[4].show();
    chart.series[5].show();
  });
  $('select.styled').customSelect();
  $("select#select1").change(function() {
    var selectVal1 = parseInt($("select#select1").val());
    var selectVal2 = parseInt($("select#select2").val());
    for(var i=0;i < chart.series.length; i++){
      if(chart.series[i].visible) {
        chart.series[i].hide();
      }
    }
    chart.series[selectVal1].show();
    chart.series[selectVal1 + 1].show();
    chart.series[selectVal2].show();
    chart.series[selectVal2 + 1].show();
  });
  $("select#select2").change(function() {
    var selectVal1 = parseInt($("select#select1").val());
    var selectVal2 = parseInt($("select#select2").val());
    for(var i=0;i < chart.series.length; i++){
      if(chart.series[i].visible) {
        chart.series[i].hide();
      }
    }
    chart.series[selectVal1].show();
    chart.series[selectVal1 + 1].show();
    chart.series[selectVal2].show();
    chart.series[selectVal2 + 1].show();
  });

});
